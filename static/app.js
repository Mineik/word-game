import {createApp} from "https://unpkg.com/petite-vue@0.4.1/dist/petite-vue.es.js"
import {nanographql as gql} from "./lib/nanographql.js";

createApp({
	currentStage: 0,
	letters: [
		["", "", "", "", ""],
		["", "", "", "", ""],
		["", "", "", "", ""],
		["", "", "", "", ""],
		["", "", "", "", ""],
		["", "", "", "", ""],
	],
	statuses: [
		["GUESSING", "GUESSING", "GUESSING", "GUESSING", "GUESSING",],
		["GUESSING", "GUESSING", "GUESSING", "GUESSING", "GUESSING",],
		["GUESSING", "GUESSING", "GUESSING", "GUESSING", "GUESSING",],
		["GUESSING", "GUESSING", "GUESSING", "GUESSING", "GUESSING",],
		["GUESSING", "GUESSING", "GUESSING", "GUESSING", "GUESSING",],
		["GUESSING", "GUESSING", "GUESSING", "GUESSING", "GUESSING",],
	],

	alphabet: [...Array('z'.charCodeAt(0) - 'a'.charCodeAt(0) + 1).keys()].map(i => {
		return{
			character: i + 'a'.charCodeAt(0),
			status: "GUESSING"
		}
	}),
	loading: false,
	endgame: false,
	popup:{
		enabled: false,
		text:"XD",
	},
	localHistory: [],
	displayGame: true,


	guess() {
		const query = gql`
            query($l1: String!,$l2: String!,$l3: String!,$l4: String!,$l5: String!, ){
                guess(
                    l1: $l1,
                    l2: $l2,
                    l3: $l3,
                    l4: $l4,
                    l5: $l5,
                ){
                    position_1
                    position_2
                    position_3
                    position_4
                    position_5
					overall_result
                }
            }
		`
		this.loading = true
		fetch("/api", {
			method: "POST",
			body: query({
				l1: this.letters[this.currentStage][0],
				l2: this.letters[this.currentStage][1],
				l3: this.letters[this.currentStage][2],
				l4: this.letters[this.currentStage][3],
				l5: this.letters[this.currentStage][4],
			}),
			headers: {
				"accept": "application/json",
				"content-type": "application/json"
			}
		}).then(e => e.json().then(({data, errors}) => {
			this.loading = false

			if(errors?.length>0){
				this.flashPopup(errors[0].message)
			}

			if (data !== null && data !== undefined) {
				console.log(data)

				this.statuses[this.currentStage][0] = data.guess.position_1
				this.statuses[this.currentStage][1] = data.guess.position_2
				this.statuses[this.currentStage][2] = data.guess.position_3
				this.statuses[this.currentStage][3] = data.guess.position_4
				this.statuses[this.currentStage][4] = data.guess.position_5

				this.statuses[this.currentStage].forEach((status, index)=>{
					const letter = this.letters[this.currentStage][index]
					const letterOfAlphabet = this.alphabet.findIndex((e)=>String.fromCharCode(e.character).valueOf()===letter)

					const alphabetStatus = this.alphabet[letterOfAlphabet].status

					switch(alphabetStatus){
						case "GUESSING":
							this.alphabet[letterOfAlphabet].status = status
							break
						case "MISPLACED":
							if(status === "CORRECT") this.alphabet[letterOfAlphabet].status = status
							break
						case "NOT_PRESENT":
						case "CORRECT":
							break
						default:
							throw alphabetStatus
					}
				})
				console.log(this.currentStage,data.guess,data.overall_result)
				if (this.currentStage < 5 && data.guess.overall_result===false) {
					this.currentStage++
				} else {
					this.loading = true
					const nQuery = gql`
                        {
							word
						}
					`
					fetch("/api", {
						method: "POST",
						headers: {
							accept: "application/json",
							"content-type":"application/json",
						},
						body: nQuery({})
					}).then(q=>q.json().then(({data:neoData, errors:neoErrors})=>{
						if(!neoErrors){
							this.flashPopup(data.guess.overall_result? "Success!":`Better luck next time! The word was "${neoData.word}".`)
							const toDate = new Date()
							if(data.guess.overall_result===true) {
								localStorage.setItem(`guessed_${toDate.getFullYear()}${toDate.getMonth()+1>10?"":"0"}${toDate.getMonth()+1}${toDate.getDate()>10?"":"0"}${toDate.getDate()}`, neoData.word)
							}else{
								localStorage.setItem(`guessed_${toDate.getFullYear()}${toDate.getMonth()+1>10?"":"0"}${toDate.getMonth()+1}${toDate.getDate()>10?"":"0"}${toDate.getDate()}`, `NOT:${neoData.word}`)
							}
							this.endgame = true
							this.loading = false
							this.reloadHistory()
						}else console.log(errors, neoErrors)
					}))
				}
			}
		}))
	},
	kbdpress(key){
		let skippable = false
		this.letters[this.currentStage].forEach((e,i)=>{
			if(e===""&&!skippable&&!this.endgame) {
				this.letters[this.currentStage][i] = String.fromCharCode(key)
				skippable = true
			}
		})
	},
	kbdbackspace(){
		let skippable = false
		this.letters[this.currentStage].forEach((e,i)=>{
			if(i>=0&&e===""&&!skippable&&!this.endgame){
				this.letters[this.currentStage][i-1] = ""
				skippable = true
			}else if(i+1 === this.letters[this.currentStage].length){
				this.letters[this.currentStage][i] = ""
			}
		})
	},

	flashPopup(text, time=2000){
		this.popup.text = text
		this.popup.enabled = true

		setTimeout(this._disablePopup, time)
	},
	_disablePopup(){
		this.popup.enabled = false
	},
	mountEvent(){
		const toDate = new Date()
		const todayGuess = localStorage.getItem(`guessed_${toDate.getFullYear()}${toDate.getMonth()>10?"":"0"}${toDate.getMonth()}${toDate.getDate()>10?"":"0"}${toDate.getDate()}`)

		if(todayGuess !== null && todayGuess !== undefined) {
			this.endgame = true
			this.flashPopup(`You have already ${todayGuess.startsWith("NOT:")?"un":""}successfully guessed the word "${todayGuess.replaceAll("NOT:","")}" today!`, 60000)
		}

		this.reloadHistory()
	},
	toggleHistory(){
		this.displayGame = !this.displayGame
	},

	reloadHistory(){
		const historyKeys = Object.keys(localStorage).filter(key=>{
			return key.startsWith("guessed_")
		})

		this.localHistory = historyKeys.map(key=>{
			const history = localStorage.getItem(key)
			const dateString = key.replace("guessed_","")
			const success = !history.startsWith("NOT:")
			const word = history.replaceAll("NOT:","")
			return {
				date: `${dateString.substring(0,4)}-${dateString.substring(4,6)}-${dateString.substring(6)}`,
				success,
				word,
			}
		})
	}
}).mount()
