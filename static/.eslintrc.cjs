module.exports={
	root: false,
	env:{
		browser: true,
		es2021: true,
		node: false
	},
	parserOptions: {
		ecmaVersion: 2021,
		sourceType: "module"
	}
}
