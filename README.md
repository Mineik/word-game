# Word game
Inspired by global hit, Wordle

## Why
I've noticed this little online/browser piece of technology called wordle being bought by new york times for low millions of US dollars.
Combined with the fact that wordle is in fact built entirely with client side javascript, I got decently mad, so I made this for fun.

## What
Simple javascript/typescript app, client is **not responsive** web app with boxes for inputing letters.
Your task is to guess a 5-letter word in 6 or less tries.

## How
### The tech stack
* Programming languages
  * Typescript (Backend)
  * Javascript (Frontend)
* Frameworks/libraries
  * Express (http framework)
  * Express-graphql (express middleware to deal with graphql)
  * petite-vue (lighter version of vue.js, without virtual DOM only supporting newest browsers)
  * *edited* nanographql (simple graphql client)
  * picocss (<10kb css framework)

### Run
* Prerequisites
  * Node >16.0 (developed on, not expected to run on any other)
  * A computer
* The running
  * `yarn` - install dependencies
  * `yarn build` - build backend
  * `yarn start` - launch the application

### Navigate the application
By default, the app runs on `localhost:3000`, on visit, you can see the web application. Nothing too fancy
The real fun is the `/api` endpoint. It features the whole graphql API
