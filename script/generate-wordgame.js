import ew from "english-words"
import {writeFileSync} from "fs";
import chalk from "chalk"

function shuffle(array) {
	let currentIndex = array.length,  randomIndex;

	// While there remain elements to shuffle...
	while (currentIndex != 0) {

		// Pick a remaining element...
		randomIndex = Math.floor(Math.random() * currentIndex);
		currentIndex--;

		// And swap it with the current element.
		[array[currentIndex], array[randomIndex]] = [
			array[randomIndex], array[currentIndex]];
	}

	return array;
}

ew.getWords(words=>{
	const candidates = words.filter(word=>{
		return word.length === 5
	})

	writeFileSync("potential-words.txt", candidates.join("\n"))
	console.log(`${chalk.greenBright("Word preparator")}:\tSaved words\t${chalk.cyan("potential-words.txt")}`)

	shuffle(candidates)

	const theGame = candidates.map((candidate, index)=>{
		if(index<365)
			return{
				date: new Date(Date.now()+((1000*3600*24)*index)),
				word: candidate
			}
		else return undefined
	}).filter(e=>e!=null)
	console.log(`${chalk.greenBright("Word preparator")}:\tPrepared words\t${chalk.cyan("365")}`)

	writeFileSync("game-script.json", JSON.stringify(theGame))
	console.log(`${chalk.greenBright("Word preparator")}:\tSaved words\t${chalk.cyan("game-script.json")}`)
})


