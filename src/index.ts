import express from "express"
import {graphqlHTTP} from "express-graphql"
import {buildSchema} from "graphql"
import cookieParser from "cookie-parser";
import chalk from "chalk";
import helmet from "helmet"
import cors from "cors"
import {resolve, join} from "path"
// @ts-ignore
import w from "../game-script.json"

const app = express()
app.disable("x-powered-by")
app.use(cookieParser())
// app.use(helmet({
// 	hsts: false,
// 	contentSecurityPolicy: false
// }))
app.use(cors({
	origin: process.env.BACKEND_ORIGIN ?? "localhost"
}))


function generateWordGameCookie<T = string>(name: string, value: T, context: express.Response) {
	context.cookie(name, value, {
		expires: new Date(new Date().setHours(23, 59)),
		httpOnly: true,
		domain: process.env.BACKEND_ORIGIN ?? "localhost",
		secure: process.env.NODE_ENV === "production",
		sameSite: "strict",
	})
}

// language=GraphQL
const schema = buildSchema(`
    type Query {
        guess(l1: String!, l2: String!, l3: String!, l4: String!, l5: String!): WordGame
        word: String
    }

    type Date {
        year: Int
        month: Int
        day: Int
    }

    enum GuessResult {
        NOT_PRESENT,
        MISPLACED,
        CORRECT
    }

    type WordGame {
        date: Date,
        position_1: GuessResult!
        position_2: GuessResult!
        position_3: GuessResult!
        position_4: GuessResult!
        position_5: GuessResult!
        overall_result: Boolean!
    }
`)

const root = {
	guess(args: {
		l1: string,
		l2: string,
		l3: string,
		l4: string
		l5: string
	}, {request, response}: { request: express.Request, response: express.Response }) {
		const {word} = w.find((word)=>{
			const d = new Date(word.date).setHours(0,0)
			const d2 = new Date().setHours(0,0)

			return new Date(d).toDateString() === new Date(d2).toDateString()
		}) ?? {date: new Date(), word:"notfound"}
		const wordleWord = word.toLowerCase()



		if ((args.l1.length == 1) && (args.l2.length == 1) && (args.l3.length == 1) && (args.l4.length == 1) && (args.l5.length == 1)) {
			const wordSplit = wordleWord.split("")
			const toDate = new Date()
			if (request.cookies.guesses === undefined) {
				console.log(`${chalk.blue("Cookie handler")}:\tCookie not set\t${chalk.cyan("trying to set it")}`)
				generateWordGameCookie<number>("guesses", 1, response)
			} else if (request.cookies.guesses == "guessed") {
				console.log(`${chalk.blue("Cookie handler")}:\tWord guessed!\t${chalk.cyan("throwing API error")}`)
				throw new Error("Word already guessed")
			} else {
				console.log(`${chalk.blue("Cookie handler")}:\tCookie set\t${chalk.cyan("incrementing")}`)
				generateWordGameCookie<number>("guesses", parseInt(request.cookies.guesses) + 1, response)
			}

			console.log(`${chalk.yellowBright("Word guess")}:\t${chalk.cyan([args.l1, args.l2, args.l3, args.l4, args.l5].join(""))}:${chalk.green(wordleWord)}\t${[args.l1, args.l2, args.l3, args.l4, args.l5].join("").valueOf() === wordleWord.valueOf() ? chalk.greenBright("Correct") : chalk.redBright("Incorrect")}`)
			if ([args.l1, args.l2, args.l3, args.l4, args.l5].join("") === wordleWord) {
				console.log(`${chalk.blue("Cookie handler")}:\tCookie set\t${chalk.cyan("Guessed")}`)
				generateWordGameCookie("guesses", "guessed", response)
			}


			return {
				date: {
					year: toDate.getFullYear(),
					month: toDate.getMonth(),
					day: toDate.getDate()
				},
				position_1: wordSplit[0] == args.l1 ? "CORRECT" : wordSplit.indexOf(args.l1) != -1 ? "MISPLACED" : "NOT_PRESENT",
				position_2: wordSplit[1] == args.l2 ? "CORRECT" : wordSplit.indexOf(args.l2) != -1 ? "MISPLACED" : "NOT_PRESENT",
				position_3: wordSplit[2] == args.l3 ? "CORRECT" : wordSplit.indexOf(args.l3) != -1 ? "MISPLACED" : "NOT_PRESENT",
				position_4: wordSplit[3] == args.l4 ? "CORRECT" : wordSplit.indexOf(args.l4) != -1 ? "MISPLACED" : "NOT_PRESENT",
				position_5: wordSplit[4] == args.l5 ? "CORRECT" : wordSplit.indexOf(args.l5) != -1 ? "MISPLACED" : "NOT_PRESENT",
				overall_result: ([args.l1, args.l2, args.l3, args.l4, args.l5].join("").valueOf()) === (wordleWord.valueOf())
			}
		} else {
			throw new Error("your guess is poorly formated")
		}
	},
	word(_args: unknown, {request: req}: { request: express.Request }) {
		if (req.cookies.guesses == "guessed" || parseInt(req.cookies.guesses) >= 6) {
			return w.find((word)=>{
				const d = new Date(word.date).setHours(0,0)
				const d2 = new Date().setHours(0,0)

				return new Date(d).toDateString() === new Date(d2).toDateString()
			})?.word
		}
		throw new Error("you haven't guessed the word yet")
	}
}

app.use("/api", graphqlHTTP((req, res) => ({
	schema,
	graphiql: process.env.NODE_ENV === "production",
	context: {
		request: req, //this is the default context I need for this to work
		response: res, //because this isn't available by default for some reason
	},
	rootValue: root,
	customFormatErrorFn: error =>{
		console.log(`${chalk.red("Express API")}:\t${chalk.redBright("GraphQL Error")}\t${error.message}`)
		return {
			message: error.message,
			locations: error.locations,
			path: error.path
		}
	}
})))

app.use(express.static("static", {
	setHeaders: (response, file_path) => {
		console.log(`${chalk.red("Express API")}:\t${chalk.blue("Static File")}\t${chalk.cyan.italic(file_path.replace(join(resolve(), "/static"), "").replace(/[\\]/g, "/"))}`)
	}
}))

app.listen(3000, () => {
	console.log(`${chalk.red("Express API")}:\t${chalk.bold.green("HTTP")} - ${chalk.greenBright("UP")}\t${chalk.cyan.underline(`${process.env.BACKEND_ORIGIN ?? "localhost"}:3000`)}`)
})
