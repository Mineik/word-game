module.exports = {
	root: true,
	parser: "@typescript-eslint/parser",
	extends: [
		"plugin:@typescript-eslint/recommended",
		"eslint:recommended",
	],
	plugins: [
		"@typescript-eslint",
	],
	rules: {
		"no-console": 0,
		"no-unused-vars": 1,
		eqeqeq: 1,
		"@typescript-eslint/explicit-boundary-types":0,
		"@typescript-eslint/ban-ts-comment": process.env.NODE_ENV==="production"?2:1,
		indent: [2, "tab"],
		"@typescript-eslint/indent": [2,"tab"]
	},
	env:{
		node: true
	}
}
